export default class RequestType {
    /**
     * Supported
     */

    /** */
    static GET = 'GET';
    static PUT = 'PUT';
    static POST = 'POST';
    static DELETE = 'DELETE';

    /**
     *  ! not (officially) supported by this lib
     *  TODO add support for these types
     * 
     */

    /** */
    static PATCH = 'PATCH';
    static HEAD = 'HEAD';
    static CONNECT = 'CONNECT';
    static OPTIONS = 'OPTIONS';
    static TRACE = 'TRACE';
}