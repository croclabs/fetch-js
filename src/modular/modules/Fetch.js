import Data from "./Data.js";
import ResponseData from "./ResponseData.js";

/**
 * Class used for sending fetch api calls
 */
export default class Fetch {
    /**
     * 
     * @param {String} url 
     * @param {Data} data 
     * @param {Function} success 
     * @param {Function} fail 
     * @param {Function} anyways 
     * @returns 
     */
    static async send(url, data, success = this.#success, fail = this.#fail, anyways = null) {
        return fetch(url, data)
            .then(async response => {
                return response.text()
                    .then(data => {
                        return new ResponseData(data, response);
                    })
                    .then(success);
            })
            .catch(fail)
            .then(anyways);
    }

    /**
     * 
     * @param {ResponseData} data 
     */
    static #success(data) {
        console.log(data.content);
        console.log(data.status);
    }

    /**
     * 
     * @param {Error} error 
     */
    static #fail(error) {
        console.warn('Something went wrong while fetching.', error);
    }
}
