/**
 * Data sent in the fetch call
 */
export default class Data {
    /** @type {String} */
    method = null;

    /** @type {String} */
    mode = 'cors';

    /** @type {String} */
    cache = 'no-cache';

    /** @type {String} */
    credentials = 'same-origin';

    /** @type {Headers} */
    headers = null;

    /** @type {String} */
    redirect = 'follow';

    /** @type {String} */
    referrerPolicy = 'no-referrer';
    
    /** @type {Object} */
    body = null;
}
