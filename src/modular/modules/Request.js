import Data from "./Data.js";
import Fetch from "./Fetch.js";
import RequestType from "./RequestType.js";

/**
 * Class used to build a Fetch request. Every setter returns this Object for further modification.
 */
export default class FetchRequest {
    /** @type {String} */
    url = "";

    /** @type {String} */
    type = null;

    /** @type {URLSearchParams} */
    params = new URLSearchParams();

    /** @type {Headers} */
    headers = new Headers();
    
    /**
     * inits a GET request
     * @returns GET request
     */
    static get() {
        return new FetchRequest().setType(RequestType.GET);
    }

    /**
     * inits a DELETE request
     * @returns DELETE request
     */
    static delete() {
        return new FetchRequest().setType(RequestType.DELETE);
    }

    /**
     * inits a post request
     * @returns POST request
     */
    static post() {
        return new BodyRequest().setType(RequestType.POST);
    }

    /**
     * inits a PUT request
     * @returns PUT request
     */
    static put() {
        return new BodyRequest().setType(RequestType.PUT);
    }

    setType(type) {
        this.type = type;
        return this;
    }

    setUrl(url) {
        this.url = url;
        return this;
    }

    getUrl() {
        if (this.params.toString() === '') {
            return this.url;
        } else {
            return this.url + '?' + this.params.toString();
        }
    }

    /**
     * Creates an object of type Data to put into a fetch api call
     * @returns Object of type Data
     */
    toData() {
        let data = new Data();

        data.method = this.type;
        data.headers = this.headers;

        return data;
    }

    /**
     * Creates the headers for this request. Possible params are either Map, Headers or an arbitrary number of strings.
     * If strings are used, they follow the following concept: key1, value1, key2, value2...
     * @param  {...any} headers 
     * @returns this object for further modifcation
     */
    addHeaders(...headers) {
        if (headers.length < 1) {
            return this;
        }

        if (headers.length == 1) {
            if (headers[0] instanceof Map ||
                headers[0] instanceof Headers) {
                headers[0].forEach((value, key) => this.addHeader(key, value));
                return this;
            }
        } else {
            this.#addHeaders(headers);
            return this;
        }

        return this;
    }

    /**
     * 
     * @param {String} key 
     * @param {String} value 
     */
    addHeader(key, value) {
        this.headers.append(key, value);
        return this;
    }

    /**
     * Creates the url params for this request. Possible params are either Map, URLSearchParams or an arbitrary number of strings.
     * If strings are used, they follow the following concept: key1, value1, key2, value2...
     * @param  {...any} params 
     * @returns this object for further modifcation
     */
    addParams(...params) {
        if (params.length < 1) {
            return this;
        }

        if (params.length == 1) {
            if (params[0] instanceof Map ||
                params[0] instanceof URLSearchParams) {
                params[0].forEach((value, key) => this.addParam(key, value));
                return this;
            }
        } else {
            this.#addParams(params);
            return this;
        }

        return this;
    }

    /**
     * 
     * @param {String} key 
     * @param {String} value 
     */
     addParam(key, value) {
        this.params.append(key, value);
        return this;
    }

    #addHeaders(headers) {
        for (let i = 0; i < headers.length; i += 2) {
            this.headers.append(headers[i], headers[i + 1]);
        }
    }

    #addParams(params) {
        for (let i = 0; i < params.length; i += 2) {
            this.params.append(params[i], params[i + 1]);
        }
    }

    /**
     * 
     * @returns an object to be used for JSON.stringify
     */
    toJSON() {
        let obj = {};

        obj.url = this.url;
        obj.type = this.type;
        obj.params = Object.fromEntries(this.params);
        obj.headers = Object.fromEntries(this.headers);

        return obj;
    }

    /**
     * 
     * @param {Function} success 
     * @param {Function} fail 
     * @param {Function} anyways 
     * @returns 
     */
    async send(success, fail, anyways) {
        return Fetch.send(this.getUrl(), this.toData(), success, fail, anyways);
    }
}

/**
 * Subclass of Request. Used for all requests that hold a body.
 */
class BodyRequest extends FetchRequest {
    /** @type {Object} */
    body = {};

    setBody(body) {
        this.body = body;
        return this;
    }

    /**
     * Creates an object of type Data to put into a fetch api call
     * @returns Object of type Data
     */
    toData() {
        let data = super.toData();
        data.body = JSON.stringify(this.body);

        return data;
    }

    /**
     * 
     * @returns an object to be used for JSON.stringify
     */
    toJSON() {
        let obj = new Object(super.toJSON());
        obj.body = this.body;

        return obj;
    }
}