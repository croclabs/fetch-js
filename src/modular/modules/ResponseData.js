export default class ResponseData {
    /** @type {String} */
    content = ''; 

    /** @type {Number} */
    status = 0;

    /** @type {Headers} */
    headers = new Headers();

    /** @type {Response} */
    response = null;

    /**
     * 
     * @param {string} content 
     * @param {Response} response 
     */
    constructor(content, response) {
        this.content = content;
        this.status = response.status;
        this.headers = response.headers;
        this.response = response;
    }
}