import FetchRequest from '../modular/modules/Request.js';
import Fetch from '../modular/modules/Fetch.js';

/**
 * Test runs for this lib
 * * uses https://reqres.in to test fetch api classes
 */

/* adding params and header via strings */
const get = FetchRequest
            .get()
            .setUrl('https://reqres.in/api/users')
            .addParams("page", "2")
            .addHeaders("header1", "value1", "header2", "value2");

/* adding params and header via JS objects */
let params = new URLSearchParams();
params.append("param1", "value1");

let headers = new Headers();
headers.append("header1", "value1");

const post = FetchRequest
            .post()
            .setUrl('https://reqres.in/api/users')
            .setBody({
                "name": "morpheus",
                "job": "leader"
            })
            .addParams(params)
            .addHeaders(headers);

const put = FetchRequest
            .put()
            .setUrl('https://reqres.in/api/users/2')
            .setBody({
                "name": "morpheus",
                "job": "zion resident"
            });

const deleteR = FetchRequest
            .delete()
            .setUrl('https://reqres.in/api/users/2');

/* fetching in order */
get.send()
.then(() => {
    post.send()
    .then(() => {
        put.send()
        .then(() => {
            deleteR.send();
        });
    });
})
.catch((error) => {
    console.warn('Something went wrong while fetching.', error);
});